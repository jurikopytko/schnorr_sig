import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.security.SecureRandom;

class KeyHolder {

    private static final int BIT_LENGTH = 1024;
    private static final int CERTAINTY = 100;

    ECPoint publicKey;
    //    MUST BE KEPT PRIVATELY
    BigInteger privateKey;
    BigInteger rnd;
    ECPoint R;

    KeyHolder(ECPoint publicKey, BigInteger privateKey, ECPoint G) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
//        needs to be replaced with better Rand impl.
        this.rnd = new BigInteger(BIT_LENGTH, CERTAINTY, new SecureRandom());
        this.R = G.multiply(rnd);
    }
}
