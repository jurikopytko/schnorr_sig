import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.crypto.params.ECKeyGenerationParameters;
import org.bouncycastle.crypto.params.ECPrivateKeyParameters;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class SignatureUtil {

    private static final String SHA_3 = "SHA3-512";

    static List<KeyHolder> generateKeyPairs(int amount, ECDomainParameters domain) {
        ECKeyPairGenerator generator = new ECKeyPairGenerator();
        ECPoint G = domain.getG();
        int counter = 1;
        List<KeyHolder> keys = new ArrayList<>(amount);
        while (counter <= amount) {
            generator.init(new ECKeyGenerationParameters(domain, new SecureRandom()));
            AsymmetricCipherKeyPair keypair = generator.generateKeyPair();
            ECPrivateKeyParameters privParams = (ECPrivateKeyParameters) keypair.getPrivate();
            ECPublicKeyParameters pubParams = (ECPublicKeyParameters) keypair.getPublic();
            keys.add(new KeyHolder(pubParams.getQ(), privParams.getD(), G));
            counter++;
        }
        return keys;
    }

    private static ECPoint combineECPoints(List<ECPoint> points) {
        ECPoint initial = points.get(0);

        for (int i = 1; i < points.size(); i++) {
            initial = initial.add(points.get(i));
        }

        return initial;
    }

    // create N of M signatures
    // two step process, public keys and random points on curve must be combined and used in hashing
    static SignatureHolder createSignature(byte[] m, int n, List<KeyHolder> keys) throws NoSuchAlgorithmException {
        Collections.shuffle(keys);
        List<KeyHolder> randomKeys = keys.subList(0, n);
        ECPoint X = combineECPoints(randomKeys.stream().map(keyHolder ->
                keyHolder.publicKey).collect(Collectors.toList()));
        ECPoint R = combineECPoints(randomKeys.stream().map(keyHolder ->
                keyHolder.R).collect(Collectors.toList()));

        MessageDigest sha3 = MessageDigest.getInstance(SHA_3);
        sha3.update(X.getEncoded(true));
        sha3.update(R.getEncoded(true));
        sha3.update(m);
        BigInteger e = new BigInteger(1, sha3.digest());

        BigInteger s = BigInteger.ZERO;
//        creating actual signature, randomKey.privateKey MUST be kept privately!!!!
        for (KeyHolder randomKey : randomKeys) {
            s = s.add(e.multiply(randomKey.privateKey).add(randomKey.rnd));
        }

        System.out.println("s " + s);
        System.out.println("R " + R);
        System.out.println("X " + X);

        return new SignatureHolder(s, R, X);
    }

    static boolean checkSignature(byte[] m, ECPoint G, SignatureHolder signature) throws NoSuchAlgorithmException {
        BigInteger leftSideEnc = new BigInteger(1, G.multiply(signature.s).getEncoded(true));
        System.out.println("left  " + leftSideEnc);
        MessageDigest sha3 = MessageDigest.getInstance(SHA_3);
        sha3.update(signature.X.getEncoded(true));
        sha3.update(signature.R.getEncoded(true));
        sha3.update(m);
        BigInteger e = new BigInteger(1, sha3.digest());

        BigInteger rightSideEnc = new BigInteger(1, signature.X.multiply(e).add(signature.R).getEncoded(true));
        System.out.println("right " + rightSideEnc);

        return leftSideEnc.equals(rightSideEnc);
    }
}
