import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;

class SignatureHolder {
    BigInteger s;
    ECPoint R;
    ECPoint X;

    SignatureHolder(BigInteger s, ECPoint R, ECPoint X) {
        this.s = s;
        this.R = R;
        this.X = X;
    }
}
