import org.bouncycastle.asn1.sec.SECNamedCurves;
import org.bouncycastle.asn1.x9.X9ECParameters;
import org.bouncycastle.crypto.params.ECDomainParameters;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.math.ec.ECCurve;
import org.bouncycastle.math.ec.ECPoint;

import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.List;

public class Main {

    public static void main(String[] args) throws
            NoSuchAlgorithmException {

        Security.addProvider(new BouncyCastleProvider());
        byte[] m = "test".getBytes();
        X9ECParameters x9ECParameters = SECNamedCurves.getByName("secp256k1");
        ECCurve curve = x9ECParameters.getCurve();
        ECPoint G = x9ECParameters.getG();
        ECDomainParameters domain = new ECDomainParameters(curve, G, x9ECParameters.getN(), x9ECParameters.getH());
        List<KeyHolder> keys = SignatureUtil.generateKeyPairs(5, domain);
        SignatureHolder signature = SignatureUtil.createSignature(m, 4, keys);
        boolean b = SignatureUtil.checkSignature(m, G, signature);
        System.out.println("signature valid: " + b);
    }
}